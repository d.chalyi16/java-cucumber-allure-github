package impl;

import api.entity.Article;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import lombok.extern.log4j.Log4j2;
import service.ArticleService;
import utils.PropertyUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static context.RunContext.RUN_CONTEXT;
import static io.restassured.RestAssured.given;
import static io.restassured.filter.log.LogDetail.BODY;

@Log4j2
public class ArticleServiceImpl implements ArticleService {
    private static final String URL = PropertyUtils.getProperties().getProperty("url");
    private static final RequestSpecification rspec =
            new RequestSpecBuilder()
                    .setContentType(ContentType.JSON)
                    .log(BODY)
                    .setBaseUri(URL)
                    .build();

    @Override
    public List<Article> getArticles(String url) {
        List<Article> articles = new ArrayList<>();

        ValidatableResponse r = given().spec(rspec).log().everything()
                .get(url)
                .then().log().ifError();

        RUN_CONTEXT.put("response", r);

        try {
            articles = r.extract().jsonPath().getList("articles.", Article.class);
        } catch (Exception e) {
            log.error("Articles request exception: " + Arrays.toString(e.getStackTrace()));
        }

        return articles;
    }
}
