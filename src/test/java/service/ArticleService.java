package service;

import api.entity.Article;

import java.util.List;

public interface ArticleService {
    List<Article> getArticles(String url);
}
