package utils;

import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.util.Properties;

@Log4j2
public final class PropertyUtils {
    public static final String URL = "url";
    private static final Properties ENV_PROPERTIES = System.getProperties();

    static {
        try {
            ENV_PROPERTIES.load(PropertyUtils.class
                    .getResourceAsStream("/properties/env.properties"));
            log.info(String.format("\n\nEnvironment: %s\n\n", Config.ENV));
        } catch (IOException e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }
    public static Properties getProperties() {
        return ENV_PROPERTIES;
    }
}